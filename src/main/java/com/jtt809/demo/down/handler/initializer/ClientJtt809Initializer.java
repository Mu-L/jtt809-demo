package com.jtt809.demo.down.handler.initializer;

import com.jtt809.demo.down.codec.ClientDecoderJtt809;
import com.jtt809.demo.down.codec.ClinetEncoderJtt809;
import com.jtt809.demo.down.handler.PrimaryLinkClientJtt809Handler;
import com.jtt809.demo.up.pojo.BasePackage;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.timeout.IdleStateHandler;
import lombok.Data;

import java.util.concurrent.TimeUnit;

@Data
public class ClientJtt809Initializer extends ChannelInitializer<SocketChannel> {

    private String ip;

    private int port;

    private String downLinkIp;

    private int downLinkPort;

    public ClientJtt809Initializer(String ip, int port, String downLinkIp, int downLinkPort) {
        this.ip = ip;
        this.port = port;
        this.downLinkIp = downLinkIp;
        this.downLinkPort = downLinkPort;
    }

    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline channelPipeline = ch.pipeline();

        //粘包分隔符
        final ByteBuf delimiter = Unpooled.buffer(1);
        delimiter.writeByte(BasePackage.MSG_END_FLAG);
        channelPipeline.addLast("delimiter", new DelimiterBasedFrameDecoder(1024, delimiter));


        // 打印日志信息
//        channelPipeline.addLast("loging", new LoggingHandler(LogLevel.INFO));

        // 解码 和 编码
        channelPipeline.addLast("decoder", new ClientDecoderJtt809());
        channelPipeline.addLast("encoder", new ClinetEncoderJtt809());

        // 心跳检测
        channelPipeline.addLast("timeout", new IdleStateHandler(0, 140, 0, TimeUnit.SECONDS));

        // 客户端的逻辑，当前仅是处理上级平台的返回信息
        channelPipeline.addLast("handler", new PrimaryLinkClientJtt809Handler(ip, port, downLinkIp, downLinkPort));
    }
}