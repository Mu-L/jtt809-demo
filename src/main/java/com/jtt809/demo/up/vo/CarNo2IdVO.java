package com.jtt809.demo.up.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>车牌号关联车辆ID</p>
 *
 * @author Allen Yang
 * @datetime 2021/9/15 15:01
 */
@Data
public class CarNo2IdVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 车牌号
     */
    private String plateNo;
}
