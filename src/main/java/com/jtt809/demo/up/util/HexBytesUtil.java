package com.jtt809.demo.up.util;

/**
 * 16进制工具类
 */
public class HexBytesUtil {

    private static final String STRING = "0123456789ABCDEF";

    /**
     * byte数组转16进制
     *
     * @param bytes
     * @return
     */
    public static String bytesToHex(byte[] bytes) {
        StringBuilder buf = new StringBuilder(bytes.length * 2);
        for (byte b : bytes) {
            // 使用String的format方法进行转换
            buf.append(String.format("%02x", new Integer(b & 0xff)));
        }
        return buf.toString();
    }

    /**
     * int转byte数组
     * @param num
     * @return
     */
    public static byte[] intToBytes(int num) {
        byte[] bytes = new byte[4];
        bytes[0] = (byte) ((num >> 24) & 0xff);
        bytes[1] = (byte) ((num >> 16) & 0xff);
        bytes[2] = (byte) ((num >> 8) & 0xff);
        bytes[3] = (byte) (num & 0xff);
        return bytes;
    }

    /**
     * 把16进制字符串转换成字节数组
     * @param hex
     * @return byte[]
     */
    public static byte[] hexStr2Bytes(String hex) {
        int len = (hex.length() / 2);
        byte[] result = new byte[len];
        char[] achar = hex.toCharArray();
        for (int i = 0; i < len; i++) {
            int pos = i * 2;
            result[i] = (byte) (toByte(achar[pos]) << 4 | toByte(achar[pos + 1]));
        }
        return result;
    }

    /**
     * 把带0x的16进制字符串转换成字节数组
     * @param hex
     * @return byte[]
     */
    public static byte[] fullHexStr2Bytes(String hex){
        hex = hex.toLowerCase().replaceAll("0x","").trim().toUpperCase();
        return hexStr2Bytes(hex);
    }

    private static int toByte(char c) {
        return (byte) STRING.indexOf(c);
    }
}
