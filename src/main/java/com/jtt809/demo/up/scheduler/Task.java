package com.jtt809.demo.up.scheduler;

import com.jtt809.demo.up.handler.PrimaryLinkServerJtt809Handler;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.util.internal.PlatformDependent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * <p>定时任务</p>
 *
 * @author Allen Yang
 * @datetime 2021/9/15 13:44
 */
@Component
@Slf4j
public class Task {

    @Resource
    private TaskProcess taskProcess;

    /**
     * <p>批量更新车辆最新定位信息
     *    每隔n毫秒执行一次</p>
     */
    @Scheduled(fixedRate = 20 * 1000)
    public void updateLocations() {
        taskProcess.updateLocations();
    }

    /**
     * <p>GPS定位时间超时未刷新则更改为离线</p>
     */
//    @Scheduled(fixedRate = 2 * 60 * 1000)
    public void updateOffline() {

    }

    /**
     * 监控内存
     */
    @Scheduled(fixedRate = 1 * 1000)
    public void monitorDirectMemory() {
        //内存快不足了，先清一波
        if ((PlatformDependent.usedDirectMemory() + PooledByteBufAllocator.DEFAULT.chunkSize()) >= PlatformDependent.maxDirectMemory()) {
            log.info("netty堆外内存监控：{}, {}, {}, {}",
                    PlatformDependent.hasUnsafe(),
                    PlatformDependent.usedDirectMemory() > PlatformDependent.maxDirectMemory(),
                    PlatformDependent.usedDirectMemory(),
                    PlatformDependent.maxDirectMemory());

            PrimaryLinkServerJtt809Handler.decrementMemoryCounter(PooledByteBufAllocator.DEFAULT.chunkSize());
        }
    }

}
