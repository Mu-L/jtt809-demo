package com.jtt809.demo.up.pojo.command.request;

import com.jtt809.demo.up.constant.ConstantJtt809;
import com.jtt809.demo.up.pojo.BasePackage;
import io.netty.buffer.ByteBuf;

/**
 * 从链路连接保持请求消息
 * 链路类型:从链路。
 * 消息方向:上级平台往下级平台。
 * 业务数据类型标识:DOWN_ LINKTEST_ REQ。
 * 描述:从链路建立成功后，上级平台向下级平台发送从链路连接保持请求消息，以保持
 * 从链路的连接状态。
 * 从链路连接保持请求消息，数据体为空。
 */
public class RequestJtt809_0x9005 extends BasePackage {

    public RequestJtt809_0x9005() {
        super(ConstantJtt809.DOWN_LINKTEST_REQ);
        this.msgBodyLength = 0;
    }

    @Override
    protected void encodeImpl(ByteBuf buf) {

    }
}
