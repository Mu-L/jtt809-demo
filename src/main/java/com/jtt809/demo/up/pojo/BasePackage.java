package com.jtt809.demo.up.pojo;

import com.jtt809.demo.up.config.Jtt809Config;
import com.jtt809.demo.up.constant.ConstantJtt809;
import com.jtt809.demo.up.util.Crc16ccittUtil;
import com.jtt809.demo.up.util.EncryptJtt809Util;
import com.jtt809.demo.up.util.TimeUtil;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.util.ReferenceCountUtil;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.nio.charset.Charset;

/**
 * <p>基础数据包</p>
 *
 * @author Allen Yang
 * @datetime 2021/9/8 17:12
 */
@Data
@Slf4j
public abstract class BasePackage {

    /**
     * 头标识
     */
    public static final short MSG_HEAD_FLAG = 0x5b;

    // region 数据头

    /**
     * 数据长度(包括头标识、数据头、数据体和尾标识)
     */
    protected long msgLength;

    /**
     * 报文序列号，自增。
     * 占用四个字节，为发送信息的序列号，用于接收方检测是否有信息的丢失，上级平台
     * 和下级平台接自己发送数据包的个数计数，互不影响。程序开始运行时等于零，发送第一帧
     * 数据时开始计数，到最大数后自动归零
     */
    private static int internalMsgNo = 0;
    protected int msgSn;

    /**
     * 业务数据类型
     */
    protected int msgId;

    /**
     * 下级平台接入码，上级平台给下级平台分配唯一标识码
     */
    protected long msgGesscenterId;

    /**
     * 协议版本好标识，上下级平台之间采用的标准协议版
     * 编号；长度为 3 个字节来表示，0x01 0x02 0x0F 标识
     * 的版本号是 v1.2.15，以此类推
     */
    protected byte[] versionFlag = {1, 0, 0};

    /**
     * 报文加密标识位 b: 0 表示报文不加密，1 表示报文加密。
     */
    protected short encryptFlag = 1;

    /**
     * 数据加密的密匙，长度为 4 个字节。
     */
    protected long encryptKey;

    // endregion

    /**
     * 数据 CRC 校验码
     */
    protected int crcCode;

    /**
     * 发送消息时的系统UTC时间，长度为 8 个字节
     */
    protected long utcTime;

    /**
     * 尾标识
     */
    public static final short MSG_END_FLAG = 0x5d;

    /**
     * 报文中除数据体外，固定的数据长度
     */
    public static short MSG_FIX_LENGTH;
    static {
        //2019版本多了个消息发送时的UTC系统时间
        if (isJtt809Version2019()) {
            MSG_FIX_LENGTH = 26 + 8;
        } else {
            MSG_FIX_LENGTH = 26;
        }
    }

    /**
     * 报文中头标识，固定的数据长度
     */
    public static final short MSG_HEAD_LENGTH = 1;

    /**
     * 报文中尾标识，固定的数据长度
     */
    public static final short MSG_END_LENGTH = 1;

    /**
     * 报文中CRC 校验码，固定的数据长度
     */
    public static final short MSG_CRC_LENGTH = 2;

    /**
     * 默认字符集
     */
    public static final Charset DEFAULT_CHARSET_GBK = Charset.forName("GBK");

    /**
     * 数据体长度
     */
    protected int msgBodyLength;

    public BasePackage() {

    }

    public BasePackage (int msgId) {
        //下行报文需要填充报文序列号
        synchronized(Integer.valueOf(internalMsgNo)) {
            if(internalMsgNo == Integer.MAX_VALUE){
                internalMsgNo = 0;
            }
        }
        this.msgSn = ++internalMsgNo;
        this.msgId = msgId;

        this.msgGesscenterId = Jtt809Config.JTT809_FACTORY_ACCESS_CODE;
        this.encryptKey = Jtt809Config.JTT809_NETTY_ENCRYPT_KEY;
    }


    // endregion

    /**
     * 数据编码
     *
     * @return
     */
    public ByteBuf encode() {
        // 创建数据包
        ByteBuf buf = Unpooled.buffer(MSG_FIX_LENGTH + msgBodyLength);

        // region 写入数据头

        /** 数据长度(包括头标识、数据头、数据体和尾标识) */
        // 4 byte
        buf.writeInt(buf.capacity());

        /** 报文序列号 */
        // 4 byte
        buf.writeInt(getMsgSn());

        /** 业务数据类型 */
        // 2 byte
        buf.writeShort(getMsgId());

        /** 下级平台接入码，上级平台给下级平台分配唯一标识码 */
        // 4 byte
        if (getMsgGesscenterId() > 0) {
            buf.writeInt((int)getMsgGesscenterId());
        } else {
            buf.writeInt(Jtt809Config.JTT809_FACTORY_ACCESS_CODE);
        }

        /** 协议版本号标识 */
        // 3 byte
        buf.writeBytes(getVersionFlag());

        /** 报文加密标识位 */
        // 1 byte
        buf.writeByte(getEncryptFlag());

        /** 数据加密的密匙 */
        // 4 byte
        if (this.getEncryptKey() > 0) {
            buf.writeInt((int) this.getEncryptKey());
        } else {
            buf.writeInt((int) Jtt809Config.JTT809_NETTY_ENCRYPT_KEY);
        }

        /**发送消息时的系统UTC时间 8byte**/
        if (isJtt809Version2019()) {
            buf.writeLong(TimeUtil.getUTCTime() / 1000);
        }

        // endregion

        // 写入数据体
        encodeImpl(buf);


        //加密-crc-转义
        ByteBuf finalBuffer = Unpooled.copiedBuffer(buf);
        //数据体加密
        if (ConstantJtt809.ENCRYPT_FLAG_PACKAGE_Y == getEncryptFlag()) {
            finalBuffer = BasePackage.encrypt(finalBuffer);
        }

        // region 写入crc校验码
        // 2 byte

        byte[] b = Unpooled.buffer(finalBuffer.readableBytes()).array();
        finalBuffer.getBytes(0, b);
        finalBuffer.writeShort(Crc16ccittUtil.toCRC16_CCITT(b));

        // endregion

        // region 报文转义
        //再转义报文

        byte[] bytes = Unpooled.copiedBuffer(finalBuffer).array();
        ByteBuf headFormatedBuffer = Unpooled.buffer(finalBuffer.readableBytes());
        messageEscaping(bytes, headFormatedBuffer);
        ByteBuf buffera = Unpooled.buffer(headFormatedBuffer.readableBytes() + 2);

        // endregion

        // 写入头标识
        // 1 byte
        buffera.writeByte(MSG_HEAD_FLAG);

        buffera.writeBytes(headFormatedBuffer);

        // 写入尾标识
        // 1 byte
        buffera.writeByte(MSG_END_FLAG);

        ReferenceCountUtil.release(headFormatedBuffer);
        ReferenceCountUtil.release(finalBuffer);

        return buffera;
    }

    /**
     * 具体指令数的据体编码实现
     *
     * @param buf
     */
    protected abstract void encodeImpl(ByteBuf buf);

    /**
     * 补全位数不够的定长参数 有些定长参数，实际值长度不够，在后面补0x00
     *
     * @param length
     * @param pwdByte
     * @return
     */
    protected byte[] getBytesWithLengthAfter(int length, byte[] pwdByte) {
        try {
            byte[] lengthByte = new byte[length];
            for (int i = 0; i < pwdByte.length; i++) {
                lengthByte[i] = pwdByte[i];
            }
            for (int i = 0; i < (length - pwdByte.length); i++) {
                lengthByte[pwdByte.length + i] = 0x00;
            }
            return lengthByte;
        } catch (Exception e) {
            log.error("补全位数不够的定长参数异常", e);
            return pwdByte;
        }
    }

    /**
     * 报文转义
     * @param bytes
     * @param formatBuffer
     */
    public static void messageEscaping(byte[] bytes, ByteBuf formatBuffer) {
        for (byte b : bytes) {
            switch (b) {
                case 0x5b:
                    byte[] formatByte0x5b = new byte[2];
                    formatByte0x5b[0] = 0x5a;
                    formatByte0x5b[1] = 0x01;
                    formatBuffer.writeBytes(formatByte0x5b);
                    break;
                case 0x5a:
                    byte[] formatByte0x5a = new byte[2];
                    formatByte0x5a[0] = 0x5a;
                    formatByte0x5a[1] = 0x02;
                    formatBuffer.writeBytes(formatByte0x5a);
                    break;
                case 0x5d:
                    byte[] formatByte0x5d = new byte[2];
                    formatByte0x5d[0] = 0x5e;
                    formatByte0x5d[1] = 0x01;
                    formatBuffer.writeBytes(formatByte0x5d);
                    break;
                case 0x5e:
                    byte[] formatByte0x5e = new byte[2];
                    formatByte0x5e[0] = 0x5e;
                    formatByte0x5e[1] = 0x02;
                    formatBuffer.writeBytes(formatByte0x5e);
                    break;
                default:
                    formatBuffer.writeByte(b);
                    break;
            }
        }
    }

    /**
     * 报文反转义
     *
     * @param bytes
     * @param formatBuffer
     */
    public static void messageInversion(byte[] bytes, ByteBuf formatBuffer) {
        for (int i = 0; i <= bytes.length; i++) {
            // 获取当前元素的下一个元素
            int nextByte = 0;

            // i 超过所能读取的字节数时，退出（假设byte数组长度为92，最大下标为91，当下标为92时，退出）
            if (i > (bytes.length - 1)) {
                break;
            }

            // i < 能读取的字节数时，获取下一个可读（假设byte数组长度为92，最大下标为91，当下标为90时，可取到91）
            if (i < (bytes.length - 1)) {
                nextByte = bytes[i + 1];
            }
            switch (bytes[i]) {
                case 0x5a:
                    byte[] formatByte0x5a = new byte[1];

                    // 0x5a 0x01 ---> 0x5b
                    if (nextByte == 0x01) {
                        formatByte0x5a[0] = 0x5b;
                        i++;
                    }

                    // 0x5a 0x02 ---> 0x5a
                    if (nextByte == 0x02) {
                        formatByte0x5a[0] = 0x5a;
                        i++;
                    }
                    formatBuffer.writeBytes(formatByte0x5a);
                    break;
                case 0x5e:
                    byte[] formatByte0x5e = new byte[1];

                    // 0x5e 0x01 ---> 0x5d
                    if (nextByte == 0x01) {
                        formatByte0x5e[0] = 0x5d;
                        i++;
                    }

                    // 0x5e 0x02 ---> 0x5e
                    if (nextByte == 0x02) {
                        formatByte0x5e[0] = 0x5e;
                        i++;
                    }
                    formatBuffer.writeBytes(formatByte0x5e);
                    break;
                default:
                    formatBuffer.writeByte(bytes[i]);
                    break;
            }
        }
    }

    /**
     * <p>数据包解密</p>
     *
     * @author Allen Yang
     * @datetime 2021/9/15 18:59
     */
    public static ByteBuf decrypt(ByteBuf byteBuf) {
        int capacity = byteBuf.capacity() - byteBuf.writableBytes();
        //byteBuf 没有尾部标识
        //头标识+数据头
        int dataLength = BasePackage.MSG_FIX_LENGTH - BasePackage.MSG_CRC_LENGTH - BasePackage.MSG_END_LENGTH;
        ByteBuf buf1 = Unpooled.buffer(dataLength);
        byteBuf.readBytes(buf1, dataLength);

        //数据体
        dataLength = capacity - buf1.readableBytes() - BasePackage.MSG_CRC_LENGTH;
        ByteBuf buf2 = null;
        if (dataLength > 0) {
            buf2 = Unpooled.buffer(dataLength);
            byteBuf.readBytes(buf2, dataLength);
            //解密
            byte[] bytes = EncryptJtt809Util.encrypt(Jtt809Config.JTT809_NETTY_ENCRYPT_M1,
                    Jtt809Config.JTT809_NETTY_ENCRYPT_IA1,
                    Jtt809Config.JTT809_NETTY_ENCRYPT_IC1,
                    Jtt809Config.JTT809_NETTY_ENCRYPT_KEY, buf2.array());
            ReferenceCountUtil.release(buf2);
            buf2 = Unpooled.copiedBuffer(bytes);
        }

        //CRC校验码+尾标识(尾标识作为分隔符在解码时已被过滤，编码时还在)
        dataLength = capacity - buf1.readableBytes();
        if (buf2 != null) {
            dataLength = dataLength  - buf2.readableBytes();
        }
        ByteBuf buf3 = Unpooled.buffer(dataLength);
        byteBuf.readBytes(buf3, dataLength);

        ReferenceCountUtil.release(byteBuf);
        byteBuf = Unpooled.buffer(capacity);
        byteBuf.writeBytes(buf1);
        ReferenceCountUtil.release(buf1);
        if (buf2 != null) {
            byteBuf.writeBytes(buf2);
            ReferenceCountUtil.release(buf2);
        }
        byteBuf.writeBytes(buf3);
        ReferenceCountUtil.release(buf3);
        return byteBuf;
    }

    /**
     * <p>数据包加密</p>
     *
     * @author Allen Yang
     * @datetime 2021/9/15 18:59
     */
    public static ByteBuf encrypt(ByteBuf byteBuf) {
        //byteBuf 数据包不包括crc 头尾标识

        int capacity = byteBuf.capacity() - byteBuf.writableBytes();
        //加密需要解密
        //数据头(数据还没有加入crc、头尾标识)
        int dataLength = BasePackage.MSG_FIX_LENGTH - BasePackage.MSG_CRC_LENGTH - BasePackage.MSG_HEAD_LENGTH - BasePackage.MSG_END_LENGTH;
        ByteBuf buf1 = Unpooled.buffer(dataLength);
        byteBuf.readBytes(buf1, dataLength);

        //数据体
        dataLength = capacity - buf1.readableBytes();
        ByteBuf buf2 = null;
        if (dataLength > 0) {
            buf2 = Unpooled.buffer(dataLength);
            byteBuf.readBytes(buf2, dataLength);
            //加密
            byte[] bytes = EncryptJtt809Util.encrypt(Jtt809Config.JTT809_NETTY_ENCRYPT_M1,
                    Jtt809Config.JTT809_NETTY_ENCRYPT_IA1,
                    Jtt809Config.JTT809_NETTY_ENCRYPT_IC1,
                    Jtt809Config.JTT809_NETTY_ENCRYPT_KEY, buf2.array());
            ReferenceCountUtil.release(buf2);
            buf2 = Unpooled.copiedBuffer(bytes);
        }

//        //CRC校验码
//        dataLength = capacity - buf1.readableBytes();
//        if (buf2 != null) {
//            dataLength = dataLength  - buf2.readableBytes();
//        }
//        ByteBuf buf3 = null;
//        if (dataLength > 0) {
//            buf3 = Unpooled.buffer(dataLength);
//            byteBuf.readBytes(buf3, dataLength);
//        }

        //重新组装数据
        ReferenceCountUtil.release(byteBuf);
        byteBuf = Unpooled.buffer(capacity);
        byteBuf.writeBytes(buf1);
        ReferenceCountUtil.release(buf1);
        if (buf2 != null) {
            byteBuf.writeBytes(buf2);
            ReferenceCountUtil.release(buf2);
        }
//        if (buf3 != null) {
//            byteBuf.writeBytes(buf3);
//            ReferenceCountUtil.release(buf3);
//        }
        return byteBuf;
    }

    public static boolean isJtt809Version2019() {
        return Jtt809Config.JTT809_VERSION == ConstantJtt809.JTT809_VERSION_2019;
    }

    public static String assistTime(int time) {
        return time >= 10 ? String.valueOf(time) : "0" + time;
    }
}
