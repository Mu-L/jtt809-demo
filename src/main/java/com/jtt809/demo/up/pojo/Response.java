package com.jtt809.demo.up.pojo;

import io.netty.buffer.ByteBuf;

/**
 * <p>响应抽象类</p>
 *
 * @author Allen Yang
 * @datetime 2021/9/8 17:06
 */
public abstract class Response extends BasePackage {

    @Override
    public ByteBuf encode() {
        return null;
    }

    @Override
    public void encodeImpl(ByteBuf buf) {
    }

    /**
     * 解码
     * @param buf
     */
    public void decode(ByteBuf buf) {
        // 跳过头标识
        buf.skipBytes(1);

        // region 读取数据头

        this.setMsgLength(buf.readUnsignedInt());
        this.setMsgSn(buf.readInt());
        this.setMsgId(buf.readUnsignedShort());
        this.setMsgGesscenterId(buf.readUnsignedInt());
        byte[] versionFlag = new byte[3];
        buf.readBytes(versionFlag);
        this.setVersionFlag(versionFlag);
        this.setEncryptFlag(buf.readUnsignedByte());
        this.setEncryptKey(buf.readUnsignedInt());

        /**发送消息时的系统UTC时间 8byte**/
        if (isJtt809Version2019()) {
            this.setUtcTime(buf.readLong());
        }

        // endregion

        // 读取数据体
        decodeImpl(buf);

        // 读取crc校验码
        this.setCrcCode(buf.readUnsignedShort());

        // 跳过尾标识 已被粘包分隔符过滤器去掉
        //buf.skipBytes(1);
    }

    /**
     * 具体数据体解码
     *
     * @param buf
     */
    protected abstract void decodeImpl(ByteBuf buf);

}
