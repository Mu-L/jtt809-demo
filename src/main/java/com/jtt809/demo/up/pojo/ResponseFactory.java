package com.jtt809.demo.up.pojo;

import com.jtt809.demo.up.constant.ConstantJtt809;
import com.jtt809.demo.up.pojo.command.response.*;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>响应类创建工厂</p>
 *
 * @author Allen Yang
 * @datetime 2021/9/8 17:07
 */
@Slf4j
public class ResponseFactory {

    private static Map<Integer, Class<? extends Response>> responseMap = new HashMap<Integer, Class<? extends Response>>();

    /**
     * 业务数据类型与bean对应的map集合
     */
    static {
        responseMap.put(ConstantJtt809.UP_CONNECT_REQ, ResponseJtt809_0x1001.class);
        responseMap.put(ConstantJtt809.UP_LINKTEST_REQ, ResponseJtt809_0x1005.class);
        responseMap.put(ConstantJtt809.DOWN_CONNECT_RSP, ResponseJtt809_0x9002.class);
        responseMap.put(ConstantJtt809.DOWN_LINKTEST_RSP, ResponseJtt809_0x9006.class);
        responseMap.put(ConstantJtt809.UP_EXG_MSG_REAL_LOCATION, ResponseJtt809_0x1202.class);
        responseMap.put(ConstantJtt809.UP_EXG_MSG_HISTORY_LOCATION, ResponseJtt809_0x1203.class);
        responseMap.put(ConstantJtt809.UP_EXG_MSG_REGISTER, ResponseJtt809_0x1201.class);
        responseMap.put(ConstantJtt809.UP_BASE_MSG_VEHICLE_ADDED_ACK, ResponseJtt809_0x1601.class);
        responseMap.put(ConstantJtt809.UP_EXG_MSG_REPORT_DRIVER_INFO_ACK, ResponseJtt809_0x120A.class);
    }

    /**
     * 创建响应数据包
     *
     * @param msgId 业务数据类型
     * @param buf   数据包
     * @return
     */
    public static Response createResponse(int msgId, ByteBuf buf) {

        // 根据业务获取响应类
        Class<? extends Response> cls = null;
        int dataType = 0;
        switch (msgId) {
            // 主链路动态信息交换消息
            case ConstantJtt809.UP_EXG_MSG:
            // 主链路静态信息交换消息
            case ConstantJtt809.UP_BASE_MSG:
                // 获取子业务类型 data_type在数据包中的位置
                dataType = buf.getUnsignedShort(BasePackage.isJtt809Version2019() ? ConstantJtt809.DATA_TYPE_INDEX_OF_PACKAGE_2019 : ConstantJtt809.DATA_TYPE_INDEX_OF_PACKAGE_2011);
                cls = responseMap.get(dataType);
                break;
            default:
                cls = responseMap.get(msgId);
        }

        if (cls == null) {
            log.error("=========>【上级平台】不支持的应答类型：0x{}", dataType > 0 ? Integer.toHexString(dataType) : Integer.toHexString(msgId));
            return null;
        }

        try {
            Response response = cls.newInstance();
            response.decode(buf);

            return response;
        } catch (Exception e) {
            log.error("=========>【上级平台】创建响应数据包异常：{}", e);
        }
        return null;
    }

}
