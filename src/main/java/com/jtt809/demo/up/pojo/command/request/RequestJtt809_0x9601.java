package com.jtt809.demo.up.pojo.command.request;

import com.jtt809.demo.up.constant.ConstantJtt809;
import io.netty.buffer.ByteBuf;

/**
 * 补发车辆静态信息请求
 * 链路类型:从链路。
 * 消息方向:上级平台向下级平台下发。
 * 业务数据类型标识:UP_BASE_MSG_VEHICLE_ADDED
 * 描述:上级平台向下级平台下发补发车辆静态信息请求
 */
public class RequestJtt809_0x9601 extends RequestJtt809_0x1600_VehiclePackage {

    public RequestJtt809_0x9601() {
        super(ConstantJtt809.DOWN_BASE_MSG_VEHICLE_ADDED);
        this.dataLength = 0;
    }

    @Override
    protected void encodeDataImpl(ByteBuf buf) {

    }
}
